import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';

import {createStore} from 'redux'
import {Provider} from 'react-redux'
import 'bootstrap/dist/css/bootstrap.css'
import MainContainer from './components/MainContainer'
import Profile from './components/Profile'
import { BrowserRouter, Route } from 'react-router-dom';

const initialState = {
    error: '',
    profile: ''
};


function testTask(state = initialState, action) {
    if (action.type === 'SET_ERROR') {
        return {
            ...state,
            error: action.payload
        };
    }
    else if (action.type === 'SET_PROFILE') {
        return {
            ...state,
            profile: action.payload
        };
    }
    return state;
}

const store = createStore(
    testTask,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);


ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <div>
                <Route exact path="/" component={MainContainer} />
                <Route exact path="/profile" component={Profile} />
            </div>
        </BrowserRouter>
    </Provider>
    , document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
