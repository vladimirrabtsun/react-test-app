import React, { Component } from 'react'
import { connect } from 'react-redux'

class Profile extends Component {

    componentWillMount() {
        this.props.onGettingProfile();

    }

    render() {
        return (
            <div className="container">
                <div className="card bg-light mt-3">
                    <div className="card-header">PROFILE</div>
                    <div className="card-body">
                        {
                            this.props.testStore.profile
                                ? (
                                    <ul className="list-unstyled">
                                        <li>Город: { this.props.testStore.profile.city }</li>
                                        <li>
                                            Языки:
                                            <ul>
                                                {this.props.testStore.profile.languages.map((language) =>
                                                    <li key={ language }>{ language }</li>
                                                )}
                                            </ul>
                                        </li>
                                        <li>
                                            Социалки:
                                            <ul>
                                                {this.props.testStore.profile.filtred.web.map((social) =>
                                                    <li key={ social.label }>{ social.label } - <a href={ social.link }>{ social.link }</a></li>
                                                )}
                                                {this.props.testStore.profile.filtred.nonWeb.map((social) =>
                                                    <li key={ social.label }>{ social.label } - <a href={ social.link }>{ social.link }</a></li>
                                                )}
                                            </ul>
                                        </li>
                                    </ul>

                                )
                                : ''
                        }
                        {
                            this.props.testStore.error
                                ? (
                                    <div className="alert alert-danger" role="alert">
                                        {this.props.testStore.error}
                                    </div>
                                )
                                : ''
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({
        testStore: state
    }),
    dispatch  => ({
        onGettingProfile: () => {
            const headers = { 'Content-Type': 'application/json' }
            const method = 'post'
            fetch('https://fast-fjord-69046.herokuapp.com/api/v1/user-info/1', { method, headers })
                .then(response => {
                    if (response.ok === false) {
                        throw new Error('Сервер недоступен.')
                    }
                    var contentType = response.headers.get('content-type')
                    if (contentType && contentType.includes('application/json')) {
                        return response.json()
                    } else {
                        throw new Error('Неверный ответ от сервера.')
                    }

                })
                .then(json => {
                    json.data.filtred = {}
                    json.data.filtred.nonWeb = json.data.social.filter(function (social) {
                        return social.label != 'web';
                    });

                    json.data.filtred.web = json.data.social.filter(function (social) {
                        return social.label == 'web';
                    });

                    dispatch({
                        type: 'SET_PROFILE',
                        payload: json.data
                    })
                })
                .catch(({ message }) => {
                    dispatch({
                        type: 'SET_ERROR',
                        payload: message
                    })
                })
        }

    })
)(Profile);
