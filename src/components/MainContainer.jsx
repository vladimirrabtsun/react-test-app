import React, { Component } from 'react'
import SigninForm from './SigninForm'

export default class MainContainer extends Component {

    render() {
        return (
            <div className="container">
                <SigninForm/>
            </div>
        )
    }
}
