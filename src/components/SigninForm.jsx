import React, { Component } from 'react'
import { connect } from 'react-redux'

class SigninForm extends Component {

    constructor(props){
        super(props)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault()
        this.props.onSignin({
            email: this.email.value,
            password: this.password.value
        })
    }

    render() {

        return (
            <div className="card bg-light mt-3">
                <form onSubmit={ this.handleSubmit }>
                    <div className="card-header">Authorization form</div>
                    <div className="card-body">
                        <h5 className="card-title">Корректные данные: email = 'test@ivcbox.com', password = '123'.</h5>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"
                                ref={ (ref) => {this.email = ref} }
                            />
                            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password"
                                ref={ (ref) => {this.password = ref} }
                            />
                        </div>
                        {
                            this.props.testStore.error
                                ? (
                                    <div className="alert alert-danger" role="alert">
                                        {this.props.testStore.error}
                                    </div>
                                )
                                : ''
                        }
                    </div>
                    <div className="card-footer text-right">
                        <button type="submit" className="btn btn-primary">Login</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default connect(
    state => ({
        testStore: state
    }),
    dispatch  => ({
        onSignin: credentials => {
            const body = JSON.stringify(credentials)
            const headers = { 'Content-Type': 'application/json' }
            const method = 'post'
            fetch('https://fast-fjord-69046.herokuapp.com/login', { method, headers, body })
                .then(response => {
                    if (response.ok === false) {
                        throw new Error('Сервер недоступен.')
                    }
                    var contentType = response.headers.get('content-type')
                    if (contentType && contentType.includes('application/json')) {
                        return response.json()
                    } else {
                        throw new Error('Неверный ответ от сервера.')
                    }

                })
                .then(json => {
                    switch (json.status) {
                        case false:
                            switch (json.message) {
                                case 'wrong data':
                                    document.getElementById('exampleInputPassword1').value = ''
                                    throw new Error('Имя пользователя или пароль введены неверно.')
                                default:
                                    throw new Error('Другая ошибка.')
                            }
                        case true:
                            window.location.replace("/profile")
                            break
                        default:
                            console.log(json)
                            throw new Error('Неверный ответ от сервера.')
                    }
                })
                .catch(({ message }) => {
                    dispatch({
                        type: 'SET_ERROR',
                        payload: message
                    })
                })
        }

    })
)(SigninForm);
